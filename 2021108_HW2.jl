x = [1,1,1,1,2,3,4,5,5,6]
y = [1,1,1,1,1,1,1,1,1,1,1,2,2,2,3,4,5,6]
global u = 0
global pro = 0
global count = 0
global ppro = zeros(0)
N = 100
x1=[rand(x) for i=1:N]    
y1=[rand(y) for i=1:N]
s =x1+y1

for j in s
    if j >= 6
        global u= u + 1
    end
    global count += 1
    global pro = u /count

    append!( ppro, pro)
end

print(pro)

using Plots    
x2 = 1:N;
y2 =(ppro,1)
p1 = plot(x2,y2,xlabel="N",lw=3, title="trending plot")
plot(p1, layout=(1:1), legend=false) 